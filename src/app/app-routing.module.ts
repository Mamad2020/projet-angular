import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AccueilComponent } from './accueil/accueil.component';
import { AProposComponent } from './a-propos/a-propos.component';
import { ServicesComponent } from './services/services.component';
import { GalerieComponent } from './galerie/galerie.component';
import { ContactsComponent } from './contacts/contacts.component';


const routes: Routes = [
  {path: 'accueil', component: AccueilComponent},
  {path: 'a-propos', component: AProposComponent},
  {path: 'services', component: ServicesComponent},
  {path: 'galerie', component: GalerieComponent},
  {path: 'contacts', component: ContactsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
